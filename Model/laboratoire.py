import prothese, empreinte

class Laboratoire:
    
    def __init__(self, nom, adresse):
        self.nom = nom
        self.adresse = adresse
        self.prothese = []

    def create_prothese(self, empreinte):
        new_prothese = prothese(empreinte.patient, self)
        self.prothese.append(prothese)
        return new_prothese