import prothese, empreinte
class Dentiste:
    
    def __init__(self, nom, adresse):
        self.nom = nom
        self.adresse = adresse
        self.prothese = []
        self.empreinte = []

    def create_empreinte(self, patient):
        new_empreinte = empreinte(patient, self)
        self.empreinte.append(new_empreinte)
        return new_empreinte