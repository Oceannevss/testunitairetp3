import unittest
import requests


# from fastapi.testclient import TestClient

# client = TestClient(app)


class Test_api(unittest.TestCase):
    def test_get_dentistes(self):
        response = requests.get("http://localhost:8000/api/bip")
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.json(), list)

    def test_get_dentistes_by_id(self):
        response = requests.get("http://localhost:8000/api/bip/3")
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data, {"id": 3, "nom": "test3", "adresse": "55 bis rue de la campagne 67000 Bourgogne"})

    def test_add_dentiste(self):
        response = requests.post("http://localhost:8000/api/bip", json={"nom": "test", "adresse": "test adresse"})
        self.assertEqual(response.status_code, 201)
        data = response.json()
        self.assert_("id" in data)
        dentiste_id = data["id"]

        response1 = requests.get("http://localhost:8000/api/bip/" + dentiste_id)
        self.assertEqual(response1.status_code, 200)

    def test_update_dentiste(self):
        response = requests.put("http://localhost:8000/api/bip/3", json={"nom": "test unitaire update"})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data[1], "test unitaire update")
        self.assertNotEqual(data[1], "test3")

    def test_delete_dentiste(self):
        response = requests.delete("http://localhost:8000/api/bip/3")
        self.assertEqual(response.status_code, 200)


# Les methodes peuvent etre tester aussi avec http://localhost:8000/docs#/
# Avec l'interface FastApi tout les crud fonctionne mais avec unittest je n'ai pas reussi a faire fonctionner le test d'ajout et de modification
