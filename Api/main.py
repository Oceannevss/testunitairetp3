from fastapi import FastAPI, APIRouter, status
import connect


app = FastAPI()
router = APIRouter()


@router.get('/')
def get_dentiste():
    dentistes = connect.get_dentiste_bdd()
    return dentistes


@router.get('/{id}')
def get_dentiste_by_id(id):
    dentiste = connect.get_dentiste_by_id_bdd(id)
    return dentiste


@router.post('/', status_code=status.HTTP_201_CREATED)
def add_dentiste(nom, adresse):
    connect.add_dentiste_bdd(nom, adresse)
    dentistes = connect.get_dentiste_bdd()
    max = len(dentistes)
    return dentistes[max - 1]


@router.put('/{id}')
def update_dentiste(id: str, nom):
    connect.update_dentiste_bdd(id, nom)
    dentiste = connect.get_dentiste_by_id_bdd(id)
    return dentiste


@router.delete('/{id}')
def delete_dentiste(id: str):
    connect.delete_dentiste_bdd(id)
    return "delete success"


app.include_router(router, tags=['bip'], prefix='/api/bip')


@app.get("/api/transportbip")
def root():
    return {"message": "Welcome to FastAPI with SQLAlchemy"}
