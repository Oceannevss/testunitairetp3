import pymysql

db = pymysql.connect(host='localhost', user='root', password='root', db='biptransport')


def get_dentiste_bdd():
    with db.cursor() as cursor:
        cursor.execute("select * from dentiste")
        output = cursor.fetchall()
        return output


def get_dentiste_by_id_bdd(id):
    with db.cursor() as cursor:
        cursor.execute("select * from dentiste where id = " + str(id))
        output = cursor.fetchone()
        return output


def add_dentiste_bdd(nom, adresse):
    with db.cursor() as cursor:
        cursor.execute("select COUNT(*) from dentiste")
        res = cursor.fetchone()
        id = res[0] + 1
        cursor.execute(
            "INSERT INTO dentiste (id, nom, adresse) values (" + str(id) + ", '" + nom + "', '" + adresse + "')")
        db.commit()
        return cursor.fetchone()


def update_dentiste_bdd(id, nom):
    with db.cursor() as cursor:
        cursor.execute("update dentiste set nom = '" + nom + "' where id = " + str(id))
        db.commit()
        return cursor.fetchone()


def delete_dentiste_bdd(id):
    with db.cursor() as cursor:
        cursor.execute("delete from dentiste where id = " + id)
        db.commit()
        return cursor.fetchone()
