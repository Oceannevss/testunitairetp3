import pymysql

db = pymysql.connect(host='localhost', user='root', password='root', db='biptransport')


def get_dentiste():
    with db.cursor() as cursor:
        cursor.execute("select * from dentiste")
        output = cursor.fetchall()
        print(output)


def add_dentiste(nom, adresse):
    with db.cursor() as cursor:
        cursor.execute("select COUNT(*) from dentiste")
        res = cursor.fetchone()
        id = res[0] + 1
        cursor.execute(
            "INSERT INTO dentiste (id, nom, adresse) values (" + str(id) + ", '" + nom + "', '" + adresse + "')")
        db.commit()


def update_dentiste(id, nom):
    with db.cursor() as cursor:
        cursor.execute("update dentiste set nom = '" + nom + "' where id = " + str(id))


def delete_dentiste(id):
    with db.cursor() as cursor:
        cursor.execute("delete from dentiste where id = " + str(id))
        db.commit()


if __name__ == "__main__":
    update_dentiste(4, "test1update")
    get_dentiste()
